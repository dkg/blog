Title: Challenge: one reproducible package a week
Date: 2015-06-04 02:31
Author: Daniel Kahn Gillmor (dkg)
Slug: challenge-one-reproducible-package-a-week

I encourage anyone interested in debian development to get involved with
[the Reproducible Builds
project](https://wiki.debian.org/ReproducibleBuilds). My own project is
to try to diagnose (and hopefully provide patches for) two
unreproducible packages a week. Maybe you can do one package a week?

Reproducible Builds is another example of the kind of audacious project
that I celebrated in [my last blog
post](https://www.debian-administration.org/users/dkg/weblog/114).

It's a fun way to learn a little bit about different parts of the
archive, and to help on an incrementally-improving process. My workflow
below is meant to encourage folks to join in. The documentation [on the
wiki](https://wiki.debian.org/ReproducibleBuilds) is certainly more
authoritative (and will be more up-to-date in the future).

My usual workflow is this:

-   Visit [the list of unreproducible packages "with no
    notes"](https://reproducible.debian.net/unstable/amd64/index_no_notes.html)
    -- these are packages that are known to not build reproducibly, but
    no one else has diagnosed.
-   Click on a package basically at random (but if you notice one that
    you are familiar with in that list, go ahead and pick it!)
-   That will take you to a page showing the [debbindiff (difference
    between binary packages)](https://tracker.debian.org/pkg/debbindiff)
    of the two builds. Sometimes, this shows an obvious difference, like
    [the difference for cloc, which shows that the man page embeds the
    build
    time](https://reproducible.debian.net/dbd/unstable/amd64/cloc_1.60-1.debbindiff.html).
    -   If you have a guess about what's wrong, but don't feel sure, pop
        over to the `#debian-reproducible` IRC channel on `irc.oftc.net`
        -- usually there are friendly people there who will discuss it
        with you.
    -   If you find a debbindiff that is completely indecipherable, go
        back and pick another package
-   Fetch the package source. I like to use `debcheckout` from the
    [devscripts](https://tracker.debian.org/pkg/devscript) package, so
    that i can work with the maintainer's revision control system of
    choice. With `cloc` above, i'd do:

        debcheckout cloc

    If that failed for some reason, I would use:

        apt-get source cloc

-   Change into the source directory you just unpacked and use your
    preferred tools (i like good old `grep` and `find`) to figure out
    where the affected file is created, and how it derives its variance.
    You might want to look through [the list of known causes of
    unreproducibility](https://reproducible.debian.net/index_issues.html)
    to see if there are any similar suggestions there.
-   If you can fix the variance, make a patch!
-   File a bug report with your diagnosis (and with your patch if you
    have one). I write my bug reports as a simple e-mail, with the
    subject line describing what i found, and with a header referring it
    to the r-b project, like this:

        Source: packagenameVersion: packageversionTags: patchSeverity: wishlistUser: reproducible-builds@lists.alioth.debian.orgUsertags: closest-usertagX-Debbugs-CC: reproducible-builds@lists.alioth.debian.org

    When choosing the “closest usertag”, i look at the ["Usertagged
    bugs" block on the R-B continuous integration
    dashboard](https://reproducible.debian.net/reproducible.html#usertagged-bugs)
    or [the wiki
    documentation](https://wiki.debian.org/ReproducibleBuilds/Contribute#How_to_report_bugs)
    to see what makes sense.

-   If you find that this is an example of one of the [known
    issues](https://reproducible.debian.net/index_issues.html), you can
    also note it in [the notes.git
    repository](https://anonscm.debian.org/cgit/reproducible/notes.git),
    or just mailing a description of what you found to [the
    reproducible-builds mailing
    list](https://lists.alioth.debian.org/cgi-bin/mailman/listinfo/reproducible-builds).

The information at [the R-B wiki
page](https://wiki.debian.org/ReproducibleBuilds) is quite detailed if
you want more info.

Finally, many many thanks to all of the people involved in the project,
and particularly to `h01ger` and `Lunar^`, who have both contributed a
ton of work, and have also made it easy to plug in and help as a
less-involved contributor. The nice automatically-updated statistics
provided by [the team's continuous integration
work](https://reproducible.debian.net/) makes it a fun game to help out.

**Tags**: [challenge](https://debian-administration.org/tag/challenge),
[reproducible
builds](https://debian-administration.org/tag/reproducible%20builds)

</p>

