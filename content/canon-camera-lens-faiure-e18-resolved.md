Title: Canon camera lens faiure (E18) -- resolved!
Date: 2009-08-03 16:57
Author: Daniel Kahn Gillmor (dkg)
Slug: canon-camera-lens-faiure-e18-resolved

I have a little old Canon SD200 point-and-shoot, which is a decent
camera. unfortunately, i've (ab)used it quite a bit, lugging it
everywhere in my pocket or in a bag and even [sending it up in a
kite](http://gallery.fifthhorseman.net/v/dkg/kite/) (and, uh, bouncing
it off the ground on the way back down sometimes).

Unsurprisingly, things occasionally fail when you subject them to harsh
conditions, and for a couple months now, the lens has failed to retract,
showing a message "`E18`" on the screen and refusing to take pictures.

It turns out i'm [not the only one with this
problem](http://www.e18error.com/repair.html), and there is actually a
[really nice guide with good
photos](http://members.shaw.ca/gregs_space/Canon_E18_repair_guide_for_SD300.pdf)
for the SD300, which is a very similar problem. I'd been putting off
fixing it until i had time to really focus, out of fear that it would
take forever, but it turned out to be a really quick fix (i did
"Procedure 1" in [the step-by-step
guide](http://members.shaw.ca/gregs_space/Canon_E18_repair_guide_for_SD300.pdf)
and it worked after about a minute of fiddling with the position
sensor), and now i have my camera back!

So thank you, Greg Toews, for your excellent documentation!
