Title: multiple USB serial adapters on a SheevaPlug
Date: 2009-04-25 02:48
Author: Daniel Kahn Gillmor (dkg)
Slug: multiple-usb-serial-adapters-on-a-sheevaplug

I just noticed Matthew Palmer's [Insane/Brilliant idea of the
day](http://www.hezmatt.org/~mpalmer/blog/general/insane_brilliant_idea_of_the_day.html):
he proposes to use large sets of USB serial adapters with a
[sheevaplug](http://www.cyrius.com/debian/kirkwood/sheevaplug/index.html)
as a cheap serial console server.

As part of upstream on [cereal](http://packages.debian.org/cereal), i
feel obliged to mention that package as a tool for managing serial
console farms like this. It's designed to run in a small footprint,
stores timestamped logs for the consoles, supports concurrent remote
access, and uses standard unix accounts (usually via ssh) to permit read
and/or write access to each port. It has saved me exactly the headaches
Matt describes many times.

However, i've had trouble getting multiple identical USB serial adapters
to persist at standard device file locations across reboot. That is, if
you have four pl2303 devices from the same manufacturer, it seems to be
a crapshoot which one will be `/dev/ttyUSB0` after you restart your
system. I could find no distinguishing data in the sysfs to get udev to
persistently key off of, anyway. if you know a way to do it, i'd be
happy to see it!

Depending on how many ports you need, another alternative would be to
use a sheevaplug with a [multiport USB-to-serial
adapter](http://www.serialgear.com/8-Port-Serial-Adapter-USBG-8X-RS232.html).
While i haven't tried this specific hardware, it would remove the need
for the hub, and potentially would mean you didn't need any extra power.
I'm assuming that this device would give you persistent port naming, but
i haven't tried it. Pricewise, it seems to be a win, too: \$100 for the
SheevaPlug and \$100 for the 8-port adapter.

**Tags**: [cereal](https://debian-administration.org/tag/cereal),
[sheevaplug](https://debian-administration.org/tag/sheevaplug)

</p>

