Title: Community Impact of OpenPGP Certificate Flooding
Date: 2019-06-28 15:00:00
Author: Daniel Kahn Gillmor (dkg)
Slug: community-impact-openpgp-cert-flooding


Community Impact of OpenPGP Certificate Flooding
------------------------------------------------

I wrote yesterday about [a recent OpenPGP certificate flooding
attack](openpgp-certificate-flooding.html), what I think it means for
the ecosystem, and how it impacted me.  This is a brief followup,
trying to zoom out a bit and think about why it affected me
emotionally the way that it did.

One of the reasons this situation makes me sad is not just that it's
more breakage that needs cleaning up, or even that my personal
identity certificate was on the receiving end.  It's that it has
impacted (and will continue impacting at least in the short term) many
different people -- friends and colleagues -- who I know and care
about.  It's not just that they may be the next targets of such a
flooding attack if we don't fix things, although that's certainly
possible.  What gets me is that they were affected *because* they know
me and communicate with me.  They had my certificate in their keyring,
or in some mutually-maintained system, and as a result of what we know
to be good practice -- regular keyring refresh -- they got burned.

Of course, they didn't get actually, physically burned.  But from
several conversations i've had over the last 24 hours, i know
personally at least a half-dozen different people who i personally
know have lost hours of work, being stymied by the failing tools, some
of that time spent confused and anxious and frustrated.  Some of them
thought they might have lost access to their encrypted e-mail messages
entirely.  Others were struggling to wrestle a suddenly non-responsive
machine back into order.  These are all good people doing other
interesting work that I want to succeed, and I can't give them those
hours back, or relieve them of that stress retroactively.

One of the points I've been driving at for years is that the goals of
much of the work I care about (confidentiality; privacy; information
security and data sovereignty; healthy communications systems) are not
individual goods.  They are interdependent, communally-constructed and
communally-defended social properties.

As an engineering community, we failed -- and as an engineer, I
contributed to that failure -- at protecting these folks in this
instance about because we left things sloppy and broken and supposedly
"good enough".

Fortunately, this failure isn't the worst situation.  There's no
arbitrary code execution, no permanent data loss (unless people get
panicked and delete everything), no accidental broadcast of secrets
that shouldn't be leaked.

And as much as this is a community failure, there are also communities
of people who have recognized these problems and have been working to
solve them.  So I'm pretty happy that good people have been working on
infrastructure that saw this coming, and were preparing for it, even
if their tools haven't been as fully implemented (or as widely
adopted) as they should be yet.  Those projects include:

 * [Autocrypt](https://autocrypt.org) -- which avoids any interaction
   with the keyserver network in favor of in-band key discovery.

 * [Web Key
   Directory](https://tools.ietf.org/html/draft-koch-openpgp-webkey-service)
   or WKD, which maps e-mail addresses to a user-controlled
   publication space for their OpenPGP Keys.
   
 * [DANE OPENPGPKEY](https://tools.ietf.org/html/rfc7929) which lets a
   domain owner publish their user's minimal OpenPGP certificates in
   the DNS directly.
 
 * [Hagrid](https://gitlab.com/hagrid-keyserver/), the implementation
   behind the [keys.openpgp.org](https://keys.openpgp.org) keyserver,
   which presents the opportunity for a [updates-only
   interface](https://lists.nongnu.org/archive/html/sks-devel/2019-02/msg00041.html)
   as well as a place for people to publish their certificates if
   their domain controller doesn't support WKD or DANE OPENPGPKEY.
   Hagrid is also an excellent first public showing for the [Sequoia
   project](https://sequoia-pgp.org/), a Rust-based implementation of
   the OpenPGP standards that hopefully we can build more tooling on
   top of in the years to come.
   
Let's keep pushing these community-driven approaches forward and get
the ecosystem to a healthier place.
