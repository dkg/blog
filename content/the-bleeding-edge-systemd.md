Title: The bleeding edge: systemd
Date: 2011-04-27 08:07
Author: Daniel Kahn Gillmor (dkg)
Slug: the-bleeding-edge-systemd

Curious about these shiny new things i keep hearing about, i set up a
test desktop system using using
[systemd](http://www.freedesktop.org/wiki/Software/systemd) as the init
system (yes, that means using
[systemd-sysv](http://packages.debian.org/systemd-sysv) in place of
[sysvinit](http://packages.debian.org/sysvinit) -- so i had to remove an
essential package for this to work).

[A system-crippling bug](http://bugs.debian.org/624276) was naturally
the result of living on the bleeding edge, but fortunately it was
resolved with a trivial patch.

In all, i'm pretty happy with some of the things that systemd seems to
get right. For example, native supervision of daemon processes, clean
process states, elimination of initscript copy-paste-isms, and
socket-based service initiation are all obvious, marked improvements
over the existing sysvinit legacy cruft.

But i'm a bit concerned about other parts. For example, all the
above-mentioned features fit really well within a tightly-tuned,
well-managed server. But systemd also appears to rely heavily on complex
userland systems like dbus and policykit that would be much more at home
on a typical desktop machine. I've never seen a well-managed server
installation that warranted either policykit or dbus. Also, given the
bug i ran into -- when PID 1 aborts due to a silly assertion, your
system is well-and-truly horked. Shouldn't a *lot* more attention to
detail be happening? I'd think that a "recover gracefully from failed
assertions" approach would be the first thing to target for a would-be
PID 1.

I'm also concerned about the Linux-centricism of systemd. I understand
that features like cgroups and reliance on the spiffiness of inotify are
part of the appeal, but i also really don't want to see us stuck with
only One Single Kernel as an option. The kFreeBSD folks (and the HURD
folks) have done a lot of work to get us close to having some level of
choice at this critical layer of infrastructure. It'd be good to see
that possibility realized, to help us avoid the creeping monoculture. I
worry that systemd's over-reliance on Linux-specific features is heading
in the wrong direction.

So my question is: why is this all being presented as a package deal?
I'd be pretty happy if i could get just the "server-side" features
without incurring the dbus/policykit/etc bloat. I already run servers
with [runit](http://smarden.org/runit) as pid 1 -- they're lean and
quite nice, but runit doesn't have systemd's socket-based initiation (or
the level of heavyweight backing that systemd seems to have picked up,
for that matter).

I understand that Lennart is resistant to UNIX's traditional "do one
thing; do it well" philosophy. I can understand some of his reasoning,
but i think he might be doing his work and his tools a disservice by
taking it this far. Wouldn't systemd be better if it was clearer how to
take advantage of parts of it without having to subscribe to the entire
thing?

Of course, i might be missing some nice ways that systemd can be
effectively tuned and pared down. But i've read [his blog posts about
systemd](http://0pointer.de/blog/projects/) and i haven't seen how to
get some of the nice features without the parts i don't want. I'd love
to be pointed to some explanations that show me how i'm wrong :)

**Tags**: [systemd](https://debian-administration.org/tag/systemd)

</p>

