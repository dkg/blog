Title: DANE OPENPGPKEY for debian.org
Date: 2019-07-09 00:00:00
Author: Daniel Kahn Gillmor (dkg)
Slug: dane-openpgpkey-for-debian.org

DANE OPENPGPKEY for debian.org
==============================

I recently announced [the publication of Web Key Directory for
`@debian.org` e-mail addresses](wkd-for-debian.org.html).  This blog post
announces another way to fetch OpenPGP certificates for `@debian.org`
e-mail addresses, this time using only the DNS.  These two mechanisms
are complementary, not in competition.  We want to make sure that
whatever certificate lookup scheme your OpenPGP client supports, you
will be able to find the appropriate certificate.

The additional mechanism we're now supporting (since a few days ago)
is DANE OPENPGPKEY, specified in [RFC
7929](https://tools.ietf.org/html/rfc7929).


How does it work?
=================

DANE OPENPGPKEY works by storing a minimized OpenPGP certificate in
the DNS, ideally in a subdomain at label based on a hashed version of
the local part of the e-mail address.

With modern GnuPG, if you're interested in retrieving the OpenPGP
certificate for `dkg` as served by the DNS, you can do:

    gpg --auto-key-locate clear,nodefault,dane --locate-keys dkg@debian.org


If you're interested in how this DNS zone is populated, take a look
[at can the code that handles
it](https://salsa.debian.org/debian-keyring/keyring/commits/publish-dane).
Please [request
improvements](https://salsa.debian.org/debian-keyring/keyring/merge_requests)
if you see ways that this could be improved.

Unfortunately, [GnuPG does not currently do DNSSEC validation on these
records](https://dev.gnupg.org/T4618), so the cryptographic
protections offered by this client are not as strong as those provided
by WKD (which at least checks the X.509 certificate for a given domain
name against the list of trusted root CAs).

Why offer both DANE OPENPGPKEY and WKD?
=======================================

I'm hoping that the Debian project can ensure that no matter whatever
sensible mechanism any OpenPGP client implements for certificate
lookup, it will be able to find the appropriate OpenPGP certificate
for contacting someone within the `@debian.org` domain.

A clever OpenPGP client might even consider these two mechanisms --
DANE OPENPGPKEY and WKD -- as corroborative mechanisms, since an
attacker who happens to compromise one of them may find it more
difficult to compromise both simultaneously.

How to update?
==============

If you are a Debian developer and you want your OpenPGP certificate
updated in the DNS, please follow [the normal procedures for Debian
keyring maintenance](https://keyring.debian.org/) like you always
have.  When a new debian-keyring package is released, we will update
these DNS records at the same time.

Thanks
======

Setting this up would not have been possible without help from
`weasel` on the Debian System Administration team, and `Noodles` from
the keyring-maint team providing guidance.

DANE OPENPGPKEY was documented and shepherded through the IETF by Paul
Wouters.

Thanks to all of these people for making it possible.
