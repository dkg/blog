Title: Magic SysRq to xen's dom0 on a serial console
Date: 2007-05-11 02:34
Author: Daniel Kahn Gillmor (dkg)
Slug: magic-sysrq-to-xens-dom0-on-a-serial-console

For a normal server running with the kernel console over a serial line,
you can get Magic SysRq behavior by sending a "break" signal, followed
by the character of the command you want triggered. How does that work
for dom0 of a Xen install?

I've just set up a xen server which is running the hypervisor and dom0
over the serial line, using debian etch xen support. I'm finding that i
can't use my typical break-character sequence to get the SysRq effect on
dom0. If i boot the machine without the xen hypervisor (standard 686
kernel) the break-character sequence works fine.

I'm having a hard time searching for this, because a lot of posts have
been made about sending Magic SysRq signals to domUs, and what i'm
looking for seems to be lost in the noise.

I confess i'm also not sure who should handle the Magic SysRq: should
the hypervisor handle it and pass it off to the dom0 kernel? Or does the
dom0 kernel handle it and hand it off to the hypervisor? confusing...

Here's my grub stanza for the machine:

    title           Xen 3.0.3-1-i386-pae / Debian GNU/Linux, kernel 2.6.18-4-xen-686root            (hd0,0)kernel          /xen-3.0.3-1-i386-pae.gz dom0_mem=131072 com1=115200,8n1module          /vmlinuz-2.6.18-4-xen-686 root=/dev/mapper/vg_monkey0-dom0 ro console=ttyS0,115200n8 module          /initrd.img-2.6.18-4-xen-686savedefault

and `/proc/sys/kernel/sysrq` is already set to `1`. Anyone have any
ideas what i should do to be able to re-enable this life-saving feature?
I know if my dom0 needs this kind of thing, i'm in bad shape already.
But that's exactly the time when i'll want it!

**Tags**: [serial
console](https://debian-administration.org/tag/serial%20console),
[sysrq](https://debian-administration.org/tag/sysrq),
[xen](https://debian-administration.org/tag/xen)

</p>

