Title: Changes for GnuPG in Debian
Date: 2016-08-03 21:55
Author: Daniel Kahn Gillmor (dkg)
Slug: changes-for-gnupg-in-debian

The [GNU Privacy Guard (GnuPG)](https://www.gnupg.org/) upstream team
maintains three branches of development: 1.4 ("classic"), 2.0
("stable"), and 2.1 ("modern").

They differ in various ways: software architecture, supported
algorithms, network transport mechanisms, protocol versions, development
activity, co-installability, etc.

Debian currently ships two versions of GnuPG in every maintained suite
-- in particular, `/usr/bin/gpg` has historically always been provided
by the "classic" branch.

That's going to change!

Debian unstable will soon be moving to the "modern" branch for providing
`/usr/bin/gpg`. This will give several advantages for Debian and its
users in the future, but it will require a transition. Hopefully we can
make it a smooth one.

What are the benefits?
----------------------

Compared to "classic", The "modern" branch has:

-   updated crypto (including elliptic curves)
-   componentized architecture (e.g. libraries, some daemonized
    processes)
-   improved key storage
-   better network access (including talking to keyservers over
    [tor](https://www.torproject.org))
-   stronger defaults
-   more active upstream development
-   safer info representation (e.g. no more key IDs, fingerprints easier
    to copy-and-paste)

If you want to try this out, the changes are already made in
experimental. Please experiment!

What does this mean for end users?
----------------------------------

If you're an end user and you don't use GnuPG directly, you shouldn't
notice much of a change once the packages start to move through the rest
of the archive.

Even if you do use GnuPG regularly, you shouldn't notice too much of a
difference. One of the main differences is that all access to your
secret key will be handled through gpg-agent, which should be
automatically launched as needed. This means that operations like
signing and decryption will cause gpg-agent to prompt the the user to
unlock any locked keys directly, rather than gpg itself prompting the
user.

If you have an existing keyring, you may also notice a difference based
on a change of how your public keys are managed, though again this
transition should ideally be smooth enough that you won't notice unless
you care to investigate more deeply.

If you use GnuPG regularly, you might want to read the NEWS file that
ships with GnuPG and related packages for updates that should help you
through the transition.

If you use GnuPG in a language other than English, please install the
`gnupg-l10n` package, which contains the localization/translation files.
For versions where those files are split out of the main package,
`gnupg` explicitly `Recommends: gnupg-l10n` already, so it should be
brought in for new installations by default.

If you have an archive of old data that depends on known-broken
algorithms, PGP3 keys, or other deprecated material, you'll need to have
"classic" GnuPG around to access it. That will be provided in the
`gnupg1` package

What does this mean for package maintainers?
--------------------------------------------

If you maintain a package that depends on `gnupg`: be aware that the
`gnupg` package in debian is going through this transition.

A few general thoughts:

-   <p>
    If your package `Depends: gnupg` for signature verification only,
    you might prefer to have it `Depends: gpgv` instead. `gpgv` is a
    much simpler tool that the full-blown GnuPG suite, and should be
    easier to manage. I'm happy to help with such a transition (we've
    made it recently with `apt` already)
-   <p>
    If your package `Depends: gnupg` and expects `~/.gnupg/` to be laid
    out in a certain way, that's almost certainly going to break at some
    point. `~/.gnupg/` is GnuPG's internal storage, and it's not
    recommended to rely on any specific data structures there, as they
    may change. `gpg` offers commands like `--export`, `--import`, and
    `--delete` for manipulating its persistent storage. please use them
    instead!
-   <p>
    If your package depends on parsing or displaying `gpg`'s output for
    the user, please make sure you use its special machine-readable form
    (`--with-colons`). Parsing the human-readable text is not advised
    and may change from version to version.

If you maintain a package that depends on `gnupg2` and tries to use
`gpg2` instead of `gpg`, that should stay ok. However, at some point
it'd be nice to get rid of `/usr/bin/gpg2` and just have one expected
binary (`gpg`). So you can help with that:

-   <p>
    Look for places where your package expects `gpg2` and make it try
    `gpg` instead. If you can make your code fall back cleanly
-   <p>
    Change your dependencies to indicate `gnupg (>= 2)`
-   <p>
    Patch `lintian` to encourage other people to make this switch ;)

What specifically needs to happen?
----------------------------------

The last major step for this transition was renaming the source package
for "classic" GnuPG to be `gnupg1`. This transition is currently in the
ftp-master's NEW queue. Once it makes it through that queue, and both
`gnupg1` and `gnupg2` have been in experimental for a few days without
reports of dangerous breakage, we'll upload both `gnupg1` and `gnupg2`
to unstable.

We'll also need to do some triage on the BTS, reassigning some reports
which are really only relevant for the "classic" branch.

Please report bugs via the BTS as usual! You're also welcome to ask
questions and make suggestions on \#debian-gnupg on irc.oftc.net, or to
mail the Debian GnuPG packaging team at
[pkg-gnupg-maint@lists.alioth.debian.org](https://lists.alioth.debian.org/cgi-bin/mailman/listinfo/pkg-gnupg-maint).

Happy hacking!

**Tags**: [gnupg](https://debian-administration.org/tag/gnupg)

</p>

