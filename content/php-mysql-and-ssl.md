Title: PHP, MySQL, and SSL?
Date: 2011-03-07 06:02
Author: Daniel Kahn Gillmor (dkg)
Slug: php-mysql-and-ssl

Is there a way to access a MySQL database over the network from PHP and
be confident that

-   the connection is actually using SSL, and
-   the server's X.509 certificate was successfully verified?

As far as i can tell, when using [the stock MySQL
bindings](http://www.php.net/manual/en/book.mysql.php), setting
[MYSQL\_CLIENT\_SSL](http://www.php.net/manual/en/mysql.constants.php#mysql.constants)
is purely advisory (i.e. it won't fail if the server doesn't advertise
SSL support). This means it won't defend against an active network
attacker performing the equivalent of
[sslstrip](http://www.thoughtcrime.org/software/sslstrip/).

Even if `MYSQL_CLIENT_SSL` was stronger than an advisory flag, i can't
seem to come up with a way to tell PHP's basic MySQL bindings the
equivalent of [the `--ssl-ca`
flag](http://dev.mysql.com/doc/refman/5.0/en/ssl-options.html#option_general_ssl-ca)
to the `mysql` client binary. Without being able to configure this, a
"man-in-the-middle" should be able to intercept the connection by
offering their own certificate on their endpoint, and otherwise relaying
the traffic. A client that does not verify the server's identity would
be none the wiser.

One option to avoid a MITM attack would be for the server to require
client-side certs via the [`REQUIRE` option for a `GRANT`
statement](http://dev.mysql.com/doc/refman/5.0/en/grant.html#id822905),
but the basic MySQL bindings for php don't seem to support that either.

PHP's [mysqli bindings](http://www.php.net/manual/en/book.mysqli.php)
(MySQL *I*mproved, i think) feature a command called
[`ssl_set()`](http://php.net/manual/en/mysqli.ssl-set.php) which appears
to allow client-side certificate support. But its documentation isn't
clear on how it handles an invalid/expired/revoked server certificate
(let alone a server that announces that it doesn't support SSL), and it
also mentions:

> This function does nothing unless OpenSSL support is enabled.

Given that debian MySQL packages don't use OpenSSL because of [licensing
incompatibilities with the
GPL](http://people.gnome.org/~markmc/openssl-and-the-gpl.html), i'm left
wondering if packages built against yaSSL support this feature. And i'm
more than a little bit leery that i have no way of telling whether my
configuration request succeeded, or whether this function just happily
did nothing because the interpreter got re-built with the wrong flags.
Shouldn't the function fail explicitly if it cannot meet the user's
request?

Meanwhile, the [PDO bindings for
MySQL](http://www.php.net/manual/en/ref.pdo-mysql.php) apparently [don't
support SSL connections at all](http://bugs.php.net/bug.php?id=48587).

What's going on here? Does no one use MySQL over the network via PHP?
Given the number of LAMP-driven data centers, this seems pretty
unlikely. Do PHP+MySQL users just not care about privacy or integrity of
their data?

Or (please let this be the case) have i just somehow missed the obvious
documentation?

**Tags**: [mysql](https://debian-administration.org/tag/mysql),
[php](https://debian-administration.org/tag/php),
[question](https://debian-administration.org/tag/question),
[security](https://debian-administration.org/tag/security),
[ssl](https://debian-administration.org/tag/ssl),
[tls](https://debian-administration.org/tag/tls)

</p>

