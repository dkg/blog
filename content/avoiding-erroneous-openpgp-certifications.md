Title: Avoiding erroneous OpenPGP certifications
Date: 2010-03-23 02:44
Author: Daniel Kahn Gillmor (dkg)
Slug: avoiding-erroneous-openpgp-certifications

i'm aware that people don't always take proper measures during mass
OpenPGP keysignings. Apparently, some keys even get signed with no one
at the keysigning present speaking for that key (for example, if the key
was submitted to the keysigning via online mechanisms beforehand, but
the keyholder failed to show up).

Unverified certifications are potentially erroneous, and erroneous
certifications are bad for the OpenPGP web of trust. Debian and other
projects rely on the OpenPGP web of trust being reasonable and healthy.
People should make a habit of doing proper verifications at keysignings.
People who make unverified certifications should probably be made aware
of better practices.

So for future keysignings, i may introduce a key to the set under
consideration and see what sort of OpenPGP certifications that key
receives. I won't pretend to hold that key in person, won't speak for
it, and it won't have my name attached to it. But it may be on the list.

Depending on the certifications received on that key (and the feedback i
get on this blog post), i'll either publish the list of wayward
certifiers, or contact the certifiers privately. Wayward certifiers
should review their keysigning practices and revoke any certifications
they did not adequately verify.

Remember, at a keysigning party, for each key:

-   Check that the fingerprint on your copy *exactly* matches the one
    claimed by the person in question
-   Check that the person in question is actually who they say they are
    (e.g. gov't ID, with a photo that looks like them, with their name
    matching the name in the key's User ID)
-   If the fingerprints don't match, or you don't have confidence in the
    name or their identity, or no one stands up to claim the key,
    there's no harm done in simply choosing to not certify the user IDs
    associated with that key. You don't even need to tell the person
    you've decided to do so.
-   Take notes in hard copy. It will help you later.

After the keysigning, when you go to actually make your OpenPGP
certifications:

-   Make sure you have the same physical document(s) that you had during
    the keysigning (no, downloading a file from the same URL is *not*
    the same thing)
-   Use your notes to decide which keys you actually want to make
    certifications over.
-   Explicitly check the fingerprints of the fetched key before you make
    any certification over any of its User IDs. If the fingerprint
    doesn't match, discard it and move on (you might want to also
    contact the person whose key it is to let them know).
-   If a key has several user IDs on it, and some of them do not match
    the person's name, simply don't certify the non-matching user IDs.
    You should certify only the user IDs you have verified.
-   If a key has a user ID with an e-mail address on it that you aren't
    absolutely sure belongs to the person in question, mail an encrypted
    copy of your certification for that User ID to the e-mail address in
    question. If they don't control that e-mail address, they won't get
    the certification, and it will never become public. `caff` (from the
    [signing-party](http://packages.debian.org/signing-party) package)
    should help you to do that.

Feedback welcome!

**Tags**:
[keysigning](https://debian-administration.org/tag/keysigning),
[openpgp](https://debian-administration.org/tag/openpgp),
[tip](https://debian-administration.org/tag/tip)

</p>

