Title: in memory of Aaron Swartz
Date: 2013-01-15 08:35
Author: Daniel Kahn Gillmor (dkg)
Slug: in-memory-of-aaron-swartz

I was upset to learn about [Aaron Swartz](http://aaronsw.com/)'s [death
last
week](http://lessig.tumblr.com/post/40347463044/prosecutor-as-bully). I
continue to be upset about his loss, and about our loss. He didn't just
show promise of great things to come in the future -- he had already
done more work for the public good than many of us will ever do. I'd
only met him IRL a couple times, but (like many others) i had
encountered him on the 'net in many places. He was a good person,
someone i didn't need to always agree with to respect.

I read Russ Allbery's [posts
about](http://www.eyrie.org/~eagle/journal/2013-01/013.html) [Aaron and
"slacktivism"](http://www.eyrie.org/~eagle/journal/2013-01/014.html)
with much appreciation. I had been ambivalent about signing [the
whitehouse.gov petition asking for the removal of the prosecutor for
overreach](https://petitions.whitehouse.gov/petition/remove-united-states-district-attorney-carmen-ortiz-office-overreach-case-aaron-swartz/RQNrG1Ck),
because I generally distrust the effectiveness of online petitions (and
offline petitions, for that matter). But Russ's analysis convinced me to
go ahead and sign it. The petition is concrete, clear (despite wanting a
grammatical proofread), and actionable.

For people willing to go beyond petition signing to civil disobedience,
[The Aaron Swartz Memorial JSTOR
Liberator](http://aaronsw.archiveteam.org/) is an option. It makes it
straightforward to potentially violate the onerous JSTOR terms of
service by re-publishing a public-domain article from
[JSTOR](http://jstor.org) to [archive.org](http://archive.org/), where
it will be accessible to anyone directly.

As someone who builds and maintains information/communications
infrastructure, i have very mixed feelings about most online civil
disobedience, since it often takes the form of a Distributed Denial of
Service (DDoS) attack of some sort. DDoS attacks of public services are
notoriously difficult to defend against without having huge resources to
throw at the problem, so encouraging participation in a DDoS often feels
a little bit like handing out cans of gasoline when you know that
*everyone* is living in a house of straw.

However, the JSTOR Liberator is not a DDoS at all -- it's simply a
facilitation of people breaking the JSTOR Terms of Service (ToS), some
of the same terms that Aaron was facing charges for violating. So it is
a well-targeted way to demonstrate that the prosecutions were
overreaching.

I wanted to take issue with one of Russ' statements, though. In [his
second post about the
situation](http://www.eyrie.org/~eagle/journal/2013-01/014.html), Russ
wrote:

> Social activism and political disobedience are important and often
> valuable things, but performing your social activism using other
> people's stuff is just rude. I think it can be a forgivable rudeness;
> people can get caught up in the moment and not realize what they're
> doing. But it's still rude, and it's still not the way to go about
> civil disobedience.

While i generally agree with Russ' thoughtful consideration of consent,
I have to take issue with this elevation of some sort of hyper-extended
property right over the moral agency that drives civil disobedience.

To use someone else's property for the sake of a just cause without
damaging the property or depriving the owner of its use is not
"forgivable rudeness" -- it's forgivable, laudable even, because it is
just. And the person using the property doesn't need to be "caught up in
the moment and not realize what they're doing" for it to be acceptable.

Civil disobedience often involves putting some level of inconvenience or
discomfort on other people, including innocent people. It might be the
friends and family of the activist who have to deal with the jail time;
it might be the drivers stuck in a traffic jam caused by a
demonstration; it might be the people forced to shop elsewhere because
the store's doors are barricaded by protestors.

All of these people could be troubled by the civil disobedience more
than MIT's network users and admins were troubled by Aaron's protest,
and that doesn't make the protests described worse or "not the way to go
about civil disobedience." The trouble highlights a more significant
injustice, and in its troubling way does what it can to help right it.

Aaron was a troublemaker, and a good one. He will be missed.

**Tags**: [aaronsw](https://debian-administration.org/tag/aaronsw)

</p>

