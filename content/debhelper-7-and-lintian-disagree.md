Title: debhelper 7 and lintian disagree
Date: 2008-05-20 16:46
Author: Daniel Kahn Gillmor (dkg)
Slug: debhelper-7-and-lintian-disagree

I'm excited by [version 7 of
debhelper](http://joey.kitenet.net/blog/entry/dh_implementation/), in
particular the opportunity for `debian/rules` minimization. I suspect
this will lower the barrier for rapid, reasonable packaging of simple
software tools in a way that should be easy to audit and maintain.

The most-minimized `debian/rules` possible with it is just (from
`dh(1)`):

    #!/usr/bin/make -f%:   dh $@

And all of the project-specific interesting bits go into nice, clean,
well-named files under `debian/`.

However, lintian (at least as of 1.23.48) complains loudly about the
minimized `debian/rules`:

    E: xdotool source: debian-rules-missing-required-target binaryE: xdotool source: debian-rules-missing-required-target binary-archE: xdotool source: debian-rules-missing-required-target binary-indepE: xdotool source: debian-rules-missing-required-target buildE: xdotool source: debian-rules-missing-required-target clean

I'm not sure the right way to proceed: should i try to manually add
overrides for each package that uses a minimized `debian/rules`? Should
lintian recognize debhelper-specific minimized `rules` files and accept
them? Should debhelper assuage lintian somehow? Or is this rules file
minimization actually not as good an idea as I think it is?

I notice that [mr](http://packages.qa.debian.org/mr), which is Joey
Hess's first example package using the minimized `rules` also [reports
the same
errors](http://lintian.debian.org/reports/maintainer/joeyh@debian.org.html#mr).

**Tags**: [debhelper](https://debian-administration.org/tag/debhelper),
[lintian](https://debian-administration.org/tag/lintian),
[packaging](https://debian-administration.org/tag/packaging)

</p>

