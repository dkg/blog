Title: Support privacy-respecting network services!
Date: 2013-09-10 06:07
Author: Daniel Kahn Gillmor (dkg)
Slug: support-privacy-respecting-network-services

[Support privacy-respecting network services! Donate to
Riseup.net!](http://www.indiegogo.com/projects/fight-the-nsa-save-privacy-help-riseup/x/4408849)

There's a
[lot](http://www.theguardian.com/world/2013/sep/05/nsa-gchq-encryption-codes-security)
of
[news](https://www.nytimes.com/2013/09/10/business/the-border-is-a-back-door-for-us-device-searches.html?hp&_r=0&pagewanted=all)
recently about some downright [orwellian
surveillance](https://www.propublica.org/special/no-warrant-no-problem-how-the-government-can-still-get-your-digital-data)
executed
[across](http://www.theguardian.com/world/2013/sep/09/nsa-spying-brazil-oil-petrobras)
the
[globe](http://www.spiegel.de/international/world/privacy-scandal-nsa-can-spy-on-smart-phone-data-a-920971.html)
by [my own government](http://www.nsa.gov/) with the [assistance of
major American
corporations](http://www.masslive.com/politics/index.ssf/2013/06/codename_prism_secret_program_data_mining.html).
The scope is huge, and the implications are depressing. It's scary and
frustrating for anyone who cares about civil society, freedom of speech,
cultural autonomy, or data sovereignty.

As bad as the situation is, though, there are groups like
[Riseup](https://riseup.net) and [May First/People
Link](https://mayfirst.org) who actively resist the data dragnet.

The good birds at [Riseup](https://riseup.net/) have been tireless
advocates for information autonomy for people and groups working for
liberatory social change for years. They have provided (and continue to
provide) impressive, well-administered infrastructure using free
software to help further these goals, and they have a [strong political
committment](https://www.riseup.net/riseup-and-government-faq) to making
a better world for all of us and to resisting strongarm attempts to turn
over sensitive data. And they provide all this expertise and
infrastructure and support on a crazy shoestring of a budget.

So if the news has got you down, or frustrated, or upset, and you want
to do something to help improve the situation, you could do a lot worse
than [sending some much-needed funds to help Riseup maintain an
expanding
infrastructure](http://www.indiegogo.com/projects/fight-the-nsa-save-privacy-help-riseup/x/4408849).
This fundraising campaign will only last a few more days, so give now if
you can!

(note: i have worked with some of the riseup birds in the past, and hope
to continue to do so in the future. I consider it critically important
to have them as active allies in our collective work toward a better
world, which is why i'm doing the unusual thing of asking for donations
for them on my blog.)
