Title: WKD for debian.org
Date: 2019-07-04 00:00:00
Author: Daniel Kahn Gillmor (dkg)
Slug: wkd-for-debian.org

WKD for debian.org
------------------

You can now fetch the OpenPGP certificate for any Debian developer who
uses an `@debian.org` e-mail address using [Web Key
Directory](https://tools.ietf.org/html/draft-koch-openpgp-webkey-service)
(WKD).

How?
====

With modern GnuPG, if you're interested in the OpenPGP certificate for
`dkg` just do:

    gpg --locate-keys dkg@debian.org

By default, this will show you any matching certificate that you
already have in your GnuPG local keyring.  But if you don't have a
matching certificate already, it will fall back to using WKD.

These certificates are extracted from the debian keyring and published
at `https://openpgpkey.debian.org/.well-known/openpgpkey/debian.org/`, as defined
in [the
WKD spec](https://tools.ietf.org/html/draft-koch-openpgp-webkey-service).  We intend to keep them up-to-date when ever the
keyring-maint team publishes a new batch of certificates.  Our tooling
uses [some repeated invocations of
`gpg`](https://salsa.debian.org/debian-keyring/keyring/merge_requests/2)
to extract and build the published tree of files.

Debian is current *not* implementing the [Web Key Directory Update
Protocol](https://tools.ietf.org/html/draft-koch-openpgp-webkey-service-08#section-4)
(and we have no plans to do so).  If you are a Debian developer and
you want your OpenPGP certificate updated in WKD, please follow [the
normal procedures for Debian keyring
maintenance](https://keyring.debian.org/) like you always have.


What about other domains?
=========================

Our update here works great for e-mail addresses in the `@debian.org`
domain, but it has no direct effect for other e-mail addresses.

However, if you have an e-mail address in a domain you control, you
can publish your own WKD.  If you would rather use an e-mail service
in a domain managed by other people, you might also be interested in
GnuPG's list of [e-mail service providers that offer
WKD](https://wiki.gnupg.org/WKD#Mail_Service_Providers_offering_WKD).


Why?
====

The SKS keyserver network has been [vulnerable to
abuse](https://tools.ietf.org/html/draft-dkg-openpgp-abuse-resistant-keystore)
for years.  The [recent certificate flooding
attacks](community-impact-openpgp-cert-flooding.html) make fetching an
OpenPGP certificate from that pool a risky operation: potentially
causing a [denial of service against
GnuPG](https://dev.gnupg.org/T4592).  In particular, [*anyone* can
flood any certificate in
SKS](https://access.redhat.com/articles/4264021) (or other common
keyservers that are not resistant to abuse).

WKD avoids the problem of certificate flooding by arbitrary third
parties.  It's not a guaranteed defense against flooding though: the
domain controller (and whoever they authorize to update the WKD) is
still capable of offering a flooded certificate via WKD.  On the plus
side, at least some WKD clients do [aggressive filtering on
certificates found via WKD](https://dev.gnupg.org/T4607#127792), which
should limit the ability of an adversary to flood a certificate in
your local keyring.

Thanks
======

Setting this up would not have been possible without help from
`weasel` and `jcristau` from the Debian System Administration team,
and `Noodles` from the keyring-maint team.

WKD was designed and implemented by Werner Koch and the GnuPG team, in
anticipation of this specific need.

Thanks to all of these people for making it possible.

What next?
==========

There's some talk about publishing similar OpenPGP certificates in the
DNS as well, using [RFC 7929
(OPENPGPKEY) records](https://tools.ietf.org/html/rfc7929), but we haven't set that up yet.
