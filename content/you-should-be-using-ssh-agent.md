Title: You should be using ssh-agent
Date: 2010-09-27 20:18
Author: Daniel Kahn Gillmor (dkg)
Slug: you-should-be-using-ssh-agent

If you're not using `ssh-agent` to authenticate yourself to SSH servers,
you should be. (i'm assuming you're already using
`PubkeyAuthentication`; if you're still using `PasswordAuthentication`,
`ChallengeResponseAuthentication` or `KbdInteractiveAuthentication`, fix
that please).

You should use `ssh-agent` for a number of reasons, actually, but the
simplest is this: when you authenticate to a text-based channel on a
remote server, you should *never* have to type anything about that
authentication into the channel that will eventually be controlled by
the remote server.

That's because a malicious server could simply accept your connection as
an anonymous connection and print out the exact prompt you're expecting.
Then, whatever you're typing goes into the remote server instead of into
your authentication scheme. and congrats, you just gave away the
passphrase for your key.

With ssh-agent, you talk first to your agent. Then, you talk to the
server and your ssh client talks to the agent. Your keys and your
passphrase are never exposed.

the second reason is that the agent is a much smaller piece of code than
the ssh client, and it doesn't talk to the network at all (unless you
force it to). It holds your key and never releases it to querying
processes; It even runs in a protected memory space so other processes
can't peek at it.

So if this protected, isolated agent is what holds your key, you're in
much better shape than if a non-protected, larger, network-active
process (the ssh client) has direct access to your secret key material.

The third reason is that it's just more convenient -- you can put a key
in your agent, and ask it to prompt you when its use is requested. you
don't actually need to re-type your passphrase each time. you can just
hit enter or type "yes".

And if that scares you security-wise then you can put the key in for a
limited period of time, as well.

(btw, you should be using the `ssh-agent` that ships with OpenSSH,
probably not the implementation offered by gnome, which [doesn't offer a
confirmation prompt](https://bugzilla.gnome.org/show_bug.cgi?id=525574),
doesn't run in protected memory space, and links in a ton more
libraries)

So how do you use the agent? It's probably already installed and running
on your computer if you run a desktop with debian or another reasonable
free operating system.

Query what keys are in your agent:

``` {.terminal}
0 dkg@pip:~$ ssh-add -lThe agent has no identities.1 dkg@pip:~$ 
```

Add a standard OpenSSH secret key to your agent, prompting for
confirmation before each use:

``` {.terminal}
0 dkg@pip:~$ ssh-add -c ~/.ssh/id_rsaEnter passphrase for /home/dkg/.ssh/id_rsa: your nice long passphrase hereIdentity added: /home/dkg/.ssh/id_rsa (/home/dkg/.ssh/id_rsa)The user must confirm each use of the key0 dkg@pip:~$ 
```

(if you drop the `-c`, you will not be prompted at each use)

Add a standard OpenSSH secret key to your agent, with a lifespan of one
hour (3600 seconds)

``` {.terminal}
0 dkg@pip:~$ ssh-add -t 3600 ~/.ssh/id_rsaEnter passphrase for /home/dkg/.ssh/id_rsa: your nice long passphrase hereIdentity added: /home/dkg/.ssh/id_rsa (/home/dkg/.ssh/id_rsa)Lifetime set to 3600 seconds0 dkg@pip:~$ 
```

(note that you can combine the `-t $SECONDS` and `-c` flags to get key
that is time-constrained and requires a confirmation prompt at each use)

Add a [monkeysphere](http://web.monkeysphere.info)-style key (an
authentication-capable subkey from your GnuPG secret keyring) to the
ssh-agent (this will prompt you for your GnuPG passphrase with a
graphical `ssh-askpass` program during this keyload, if such a program
is available), for one hour:

``` {.terminal}
0 dkg@pip:~$ monkeysphere subkey-to-ssh-agent -t 3600Identity added: Daniel Kahn Gillmor <dkg@fifthhorseman.net> (Daniel Kahn Gillmor <dkg@fifthhorseman.net>)Lifetime set to 3600 seconds0 dkg@pip:~$ 
```

If you don't already have such a subkey, but you want to use the
monkeysphere, you'll need to run `monkeysphere gen-subkey` to create one
first.

Note also that you can use both `-c` and `-t $SECONDS` with
`monkeysphere subkey-to-ssh-agent`, just like they are used with
`ssh-add`.

Remove all keys from your running agent:

``` {.terminal}
0 dkg@pip:~$ ssh-add -DAll identities removed.0 dkg@pip:~$ 
```

I hope this is helpful to people!

**Tags**: [security](https://debian-administration.org/tag/security),
[ssh](https://debian-administration.org/tag/ssh),
[ssh-agent](https://debian-administration.org/tag/ssh-agent),
[tip](https://debian-administration.org/tag/tip)

</p>

