Title: registrars and AAAA glue records
Date: 2010-11-17 23:43
Author: Daniel Kahn Gillmor (dkg)
Slug: registrars-and-aaaa-glue-records

i've been on an IPv6 kick recently, getting dual-stack systems up and
working for a bunch of folks.

I'd like to make some of these services reachable by IPv6-only clients.
this suggests that i need a range of details sorted out, but i think the
one piece left for me is the glue records for the nameservice. i use
in-bailiwick nameservers for DNS where possible, which means i want
mandatory glue records. that is, the primary namserver for `example.org`
is probably something like `ns0.example.org`, which means that the `org`
nameservers themselves need to store not only the `NS` record, but an
`A` record that corresponds to the name pointed to by the `NS`.

But for IPv6-only clients that do their own name resolution, i need
`AAAA` glue records, and i haven't yet found a registrar that will push
`AAAA` glue records *for the same names as the existing `A` glue* into
the `org` zone.

Do you know of a registrar that will do this?

I've tried:

[dotster](https://dotster.com/)
:   Dotster seems to only allow IPv4 glue to be entered on their
    [Register Nameserver config
    page](https://secure.dotster.com/account/nameserver/registerns.php)
    (needs a dotster login to see it). They haven't yet yet responded to
    my query through [their support web
    form](https://secure.dotster.com/help/csupport.php) about submitting
    `AAAA` glue

[gandi](https://www.gandi.net)
:   gandi at least offers the opportunity to enter `AAAA` glue, but
    apparently can't let me have both `AAAA` and `A` glue for the same
    name. A note to their support team got me a response that this is
    planned “for Q1 or Q2 of 2011”.

Any suggestions for reasonable registrars that offer this today?

Am i being silly in wanting `AAAA` and `A` glue for the same names? i
note that the root zone and the `org` zone both offer `A` and `AAAA`
records for each of their dual-stack nameservers. You can check for
yourself:

     dig @a.root-servers.net ns org dig @a.root-servers.net ns .

if i don't go for dual records, i could instead use gandi and go with
distinct names for the v6 and v4 servers, like this:

    ;; QUESTION SECTION:;example.org.             IN  NS;; AUTHORITY SECTION:example.org.      172800    IN  NS  a.ns.example.org.example.org.      172800    IN  NS  b.ns.example.org.example.org.      172800    IN  NS  c.ns.example.org.example.org.      172800    IN  NS  d.ns.example.org.;; ADDITIONAL SECTION:a.ns.example.org. 172800    IN  A   192.0.2.3b.ns.example.org. 172800    IN  A   192.0.2.4c.ns.example.org. 172800    IN  AAAAA   2001:db8::3d.ns.example.org. 172800  IN  AAAAA   2001:db8::4

But of course what i really want is this:

    ;; QUESTION SECTION:;example.org.                IN  NS;; AUTHORITY SECTION:example.org.      172800    IN  NS  a.ns.example.org.example.org.      172800    IN  NS  b.ns.example.org.;; ADDITIONAL SECTION:a.ns.example.org. 172800    IN  A   192.0.2.3a.ns.example.org. 172800    IN  AAAAA   2001:db8::3b.ns.example.org. 172800  IN  A   192.0.2.4b.ns.example.org. 172800    IN  AAAAA   2001:db8::4

My concern about this is if some IPv4-only system gets a list like the
first one, and decides to use `c.ns.example.org` or `d.ns.example.org`,
which doesn't have an `A` record at all. That would be a silly
implementation, of course. but uh, we have a lot of silly
implementations of things out there.

Feedback welcome!

**Tags**: [dns](https://debian-administration.org/tag/dns),
[ipv6](https://debian-administration.org/tag/ipv6)

</p>

