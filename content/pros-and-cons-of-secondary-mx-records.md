Title: Pros and Cons of secondary MX records
Date: 2006-10-21 19:10
Author: Daniel Kahn Gillmor (dkg)
Slug: pros-and-cons-of-secondary-mx-records

Many sites use multiple MX records in DNS. But i feel like i'm seeing
more and more which just have a single MX record. Why choose the one
strategy over the other?

Given that MTAs are increasingly complicated these days (with various
spam filtering techniques), what are some good arguments for (or
against) having multiple MX records for a relatively small domain
(&lt;1000 users)? Here's a couple notes of my own (which i'm not wedded
to: please tell me if you disagree!):

For:

-   more control over mail delivery: if your primary MTA is down or
    unreachable, you still have a machine you control who will accept
    mail deliveries on your behalf, rather than trusting the remote
    mailer to retry properly.
-   it's "the standard" way to do things.
-   redundancy is good.

Against:

-   synchronizing settings between primary and secondary MTAs is
    complicated and potentially error-prone. If settings are not
    synchronized, the secondary MX could end up accepting messages for
    delivery that the primary would not have accepted.
-   simplicity is good.
-   queues on the secondary MX provide yet another place for mail to be
    lost or mangled in an already-complicated protocol
-   i've heard many reports of spammers preferring the secondary mail
    exchangers over the primaries, though i'm not clear why that is.

Your thoughts?

</p>

