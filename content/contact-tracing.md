Title: Tech-assisted Contact-Tracing against the COVID-19 pandemic
Date: 2020-04-16 20:51
Authors: Daniel Kahn Gillmor
Tags: covid-19
Summary: What are the risks and potential benefits of contact-tracing?


Today at the ACLU, we [released a
whitepaper](https://www.aclu.org/report/aclu-white-paper-principles-technology-assisted-contact-tracing)
discussing how to evaluate some novel cryptographic schemes that are
being considered to provide technology-assisted contact-tracing in the
face of the COVID-19 pandemic.

The document offers guidelines for thinking about potential schemes
like this, and what kinds of safeguards we need to expect and demand
from these systems so that we might try to address the (hopefully
temporary) crisis of the pandemic without also creating a permanent
crisis for civil liberties.

The proposals that we're seeing (including [PACT](https://pact.mit.edu),
[DP^3T](https://github.com/DP-3T), [TCN](https://tcn-coalition.org),
and the [Apple/Google
proposal](https://www.apple.com/covid19/contacttracing)) work in pretty
similar ways, and the challenges and tradeoffs there are remarkably
similar to [Internet protocol](https://ietf.org) design decisions.
Only now in addition to bytes and packets and questions of efficiency,
privacy, and control, we're also dealing directly with risk of
physical harm (who gets sick?), society-wide allocation of scarce and
critical resources (who gets tested? who gets treatment?), and
potentially serious means of exercising powerful social control (who
gets forced into quarantine?).

My ACLU colleague [Jon Callas](https://twitter.com/joncallas) and i
will be doing a Reddit
[AMA](https://dkg.fifthhorseman.net/reddit-ama-2020-04-17.jpg) in
[r/Coronavirus](https://reddit.com/r/Coronavirus) tomorrow starting at
2020-04-17T19:00:00Z (that's 3pm Friday in `TZ=America/New_York`)
about this very topic, if that's the kind of thing you're into.
