Title: Kevin M. Igoe should step down from CFRG Co-chair
Date: 2013-12-21 22:55
Author: Daniel Kahn Gillmor (dkg)
Slug: kevin-m-igoe-should-step-down-from-cfrg-co-chair

I've [said
recently](https://www.debian-administration.org/users/dkg/weblog/102)
that pervasive surveillance is wrong. I don't think anyone from the NSA
should have a leadership position in the development or deployment of
Internet communications, because their interests are at odds with the
interest of the rest of the Internet. But someone at the NSA is in
exactly such a position. They ought to step down.

Here's the background:

The [Internet Research Task Force (IRTF)](https://www.irtf.org) is a
body tasked with research into underlying concepts, themes, and
technologies related to the Internet as a whole. They act as a research
organization that cooperates and complements the engineering and
standards-setting activities of the [Internet Engineering Task Force
(IETF)](https://www.ietf.org).

The IRTF is divided into issue-specific research groups, each of which
has a Chair or Co-Chairs who have "[wide discretion in the conduct of
Research Group
business](http://wiki.tools.ietf.org/html/rfc2014#section-5.3)", and are
tasked with organizing the research and discussion, ensuring that the
group makes progress on the relevant issues, and communicating the
general sense of the results back to the rest of the IRTF and the IETF.

One of the IRTF's research groups specializes in cryptography: the
[Crypto Forum Research Group (CFRG)](https://www.irtf.org/cfrg). There
are two current chairs of the CFRG: David McGrew `<mcgrew@cisco.com>`
and Kevin M. Igoe `<kmigoe@nsa.gov>`. As you can see from his e-mail
address, Kevin M. Igoe is affiliated with the [National Security Agency
(NSA)](https://en.wikipedia.org/wiki/National_Security_Agency). The NSA
itself actively tries to weaken cryptography on the Internet so that
they can improve their surveillance, and one of the ways they try to do
so is to "[influence policies, standards, and
specifications](http://www.nytimes.com/interactive/2013/09/05/us/documents-reveal-nsa-campaign-against-encryption.html?_r=0)".

On the CFRG list yesterday, [Trevor Perrin](http://trevp.net/)
[requested the removal of Kevin M. Igoe from his position as Co-chair of
the
CFRG](https://www.ietf.org/mail-archive/web/cfrg/current/msg03554.html).
Trevor's specific arguments rest heavily on the technical merits of a
proposed cryptographic mechanism called [Dragonfly key
exchange](https://tools.ietf.org/html/draft-irtf-cfrg-dragonfly), but I
think the focus on Dragonfly itself is the least of the concerns for the
IRTF.

I've [seconded Trevor's
proposal](https://www.ietf.org/mail-archive/web/cfrg/current/msg03570.html),
and asked
[Kevin](https://debian-administration.org/weblog/feeds/kmigoe@nsa.gov)
directly to step down and to provide us with information about any
attempts by the NSA to interfere with or subvert recommendations coming
from these standards bodies.

Below is my letter in full:

>     From: Daniel Kahn Gillmor <dkg@fifthhorseman.net>To: cfrg@ietf.org, Kevin M. Igoe <kmigoe@nsa.gov>Date: Sat, 21 Dec 2013 16:29:13 -0500Subject: Re: [Cfrg] Requesting removal of CFRG co-chairOn 12/20/2013 11:01 AM, Trevor Perrin wrote:> I'd like to request the removal of Kevin Igoe from CFRG co-chair.Regardless of the conclusions that anyone comes to about Dragonflyitself, I agree with Trevor that Kevin M. Igoe, as an employee of theNSA, should not remain in the role of CFRG co-chair.While the NSA clearly has a wealth of cryptographic knowledge andexperience that would be useful for the CFRG, the NSA is apparentlyengaged in a series of attempts to weaken cryptographic standards andtools in ways that would facilitate pervasive surveillance ofcommunication on the Internet.The IETF's public position in favor of privacy and security rightlyidentifies pervasive surveillance on the Internet as a serious problem:https://www.ietf.org/media/2013-11-07-internet-privacy-and-security.htmlThe documents Trevor points to (and others from similar stories)indicate that the NSA is an organization at odds with the goals of the IETF.While I want the IETF to continue welcoming technical insight anddiscussion from everyone, I do not think it is appropriate for anyonefrom the NSA to be in a position of coordination or leadership.----Kevin, the responsible action for anyone in your position is toacknowledge the conflict of interest, and step down promptly from theposition of Co-Chair of the CFRG.If you happen to also subscribe to the broad consensus described in theIETF's recent announcement -- that is, if you care about privacy andsecurity on the Internet -- then you should also reveal any NSA activityyou know about that attempts to subvert or weaken the cryptographicunderpinnings of IETF protocols.Regards,   --dkg

I'm aware that an abdication by Kevin (or his removal by the IETF chair)
would probably not end the NSA's attempts to subvert standards bodies or
weaken encryption. They could continue to do so by subterfuge, for
example, or by private influence on other public members. We may not be
able to stop them from doing this in secret, and the knowledge that they
may do so seems likely to cast a pall of suspicion over any IETF and
IRTF proceedings in the future. This social damage is serious and
troubling, and it marks yet another cost to the NSA's reckless
institutional disregard for civil liberties and free communication.

But even if we cannot rule out private NSA influence over standards
bodies and discussion, we can certainly explicitly reject any public
influence over these critical communications standards by members of an
institution so at odds with the core principles of a free society.

Kevin M. Igoe, please step down from the CFRG Co-chair position.

And to anyone (including Kevin) who knows about specific attempts by the
NSA to undermine the communications standards we all rely on: please
blow the whistle on this kind of activity. Alert a friend, a colleague,
or a journalist. Pervasive surveillance is an attack on all of us, and
those who resist it are heroes.
