Title: success with goodbye-microsoft.com
Date: 2008-01-31 18:12
Author: Daniel Kahn Gillmor (dkg)
Slug: success-with-goodbye-microsoftcom

I finally got to try out
[goodbye-microsoft.com](http://goodbye-microsoft.com/) this past
weekend. It uses similar principles as UNetbootin, which [Utumno
recently wrote
about](https://debian-administration.org/users/Utumno/weblog/34). I had
a donated machine in a computer lab i volunteer at with a flakey CD-ROM
which i couldn't get to netboot. Since it had Windows already on it, i
booted to Windows, hooked into the network, downloaded [a debian
installer for Windows](http://goodbye-microsoft.com/pub/debian.exe), ran
it, and was on my way.

The project is clean, simply done, and provides a good interface in the
leadup to rebooting into a debian installer. I highly recommend it for
folks who need to switch a 'doze machine to the One True OS.

My one concern with this approach is if the debian install fails midway
through, (and you haven't done any gymnastics to preserve a dual-boot
scenario) you could be left with an unbootable machine. But the debian
installer is pretty much rock solid these days, especially with common
commodity hardware, so you'd probably have to trip over the power cord
to get caught out like that.

**Tags**: [windows](https://debian-administration.org/tag/windows)

</p>

