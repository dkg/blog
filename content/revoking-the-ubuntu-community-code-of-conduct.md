Title: Revoking the Ubuntu Community Code of Conduct
Date: 2009-10-20 18:24
Author: Daniel Kahn Gillmor (dkg)
Slug: revoking-the-ubuntu-community-code-of-conduct

I've just [revoked my signature over the Ubuntu Code of Conduct
1.0.1](http://fifthhorseman.net/ubuntu/coc/coc-revoked.txt). I did this
because Ubuntu's CoC (perhaps jokingly?) singles out Mark Shuttleworth
as someone who should be held to a super-human standard (as [pointed out
recently by Rhonda](http://rhonda.deb.at/blog/debian/coc-joke.en.html),
as well as earlier in [ubuntu bug
53848](https://launchpad.net/bugs/53843)).

I think that the CoC is a good document, and good guidelines in general
for reasonable participation in online communities. When i originally
signed the document, i thought the Shuttleworth-exceptionalism was odd,
but decided i'd be willing to hold him to a higher standard than the
rest of the community, if he wanted me to. That is, i figured his
position as project leader meant that he could have made the CoC
different than it is, thus he was (perhaps indirectly) asking me to hold
him to a higher standard.

Why does this matter to me now? Shuttleworth has [apparently signed the
Ubuntu Code of Conduct](https://launchpad.net/~sabdfl#ubuntu-coc), but
[as i wrote about
earlier](https://debian-administration.org/users/dkg/weblog/54), his
[recent sexist comments at
LinuxCon](http://blog.linuxtoday.com/blog/2009/09/mark-shuttlewor-1.html)
were a Bad Thing for the community, and his apparent lack of an apology
or open discussion with the community concerned about it was even worse.

So i'm asking Mark Shuttleworth to abide by the following points in the
Code of Conduct that he has signed:

-   Be considerate
-   Be respectful \[...\] It's important to remember that a community
    where people feel uncomfortable or threatened is not a productive
    one.
-   The important goal is not to avoid disagreements or differing views
    but to resolve them constructively. You should turn to the community
    and to the community process to seek advice and to resolve
    disagreements.
-   When you are unsure, ask for help. Nobody knows everything, and
    nobody is expected to be perfect in the Ubuntu community

I've [signed a revised version of the Ubuntu Code of Conduct
1.01](http://fifthhorseman.net/ubuntu/coc/coc-revised.txt) (with the
Shuttleworth-exceptionalism clause removed), to reaffirm my commitment
to these principles, and to acknowledge that, yes, the SABDFL can make a
mistake, and to encourage him to address his mistakes in a fashion
befitting a mature participant in this community we both care about.

**UPDATE:** It seems that [Mako](http://mako.cc/) and [Daniel
Holbach](http://daniel.holba.ch/) have recently revised the CoC
resulting in [a new version (1.1) which has just been approved by the
the Ubuntu Community
Council](http://mako.cc/copyrighteous/20091020-00.comment). The [new
version 1.1]{} looks good to me (i like its broadening of scope beyond
developers, and its lack of superhuman claims for Shuttleworth) and when
it is available on Launchpad, i'll most likely sign it there. Thanks to
the two of them for their work! I hope Shuttleworth will consider
abiding by this new version.

**Tags**: [code of
conduct](https://debian-administration.org/tag/code%20of%20conduct),
[sexism](https://debian-administration.org/tag/sexism),
[ubuntu](https://debian-administration.org/tag/ubuntu)

</p>

