Title: Unaccountable surveillance is wrong
Date: 2013-10-08 20:12
Author: Daniel Kahn Gillmor (dkg)
Slug: unaccountable-surveillance-is-wrong

As [I mentioned
earlier](https://debian-administration.org/users/dkg/weblog/99), the
information in the documents released by Edward Snowden show a clear
pattern of corporate and government abuse of the information networks
that are now deeply intertwined with the lives of many people all over
the world.

Surveillance is a power dynamic where the party doing the spying has
power over the party being surveilled. The surveillance state that
results when one party has "[Global Cryptologic
Dominance](http://www.nsa.gov/about/values/index.shtml)" is a seriously
bad outcome. The old saw goes "power corrupts, and absolute power
corrupts absolutely". In this case, the stated goal of my government
appears to be absolute power in this domain, with no constraint on the
inevitable corruption. If you are a supporter of any sort of a just
social contract (e.g. [International Principles on the Application of
Human Rights to Communications
Surveillance](https://necessaryandproportionate.org/)), the situation
should be deeply disturbing.

One of the major sub-threads in this discussion is how the NSA and their
allies have actively tampered with and weakened the cryptographic
infrastructure that everyone relies on for authenticated and
confidential communications on the 'net. This kind of malicious work
puts everyone's communication at risk, not only those people who the NSA
counts among their "targets" (and the NSA's "target" selection methods
are themselves fraught with serious problems).

The US government is supposed to take pride in the checks and balances
that keep absolute power out of any one particular branch. One of the
latest attempts to simulate "checks and balances" was [the President's
creation](http://www.whitehouse.gov/the-press-office/2013/08/12/presidential-memorandum-reviewing-our-global-signals-intelligence-collec)
of [a "Review
Group"](http://www.whitehouse.gov/the-press-office/2013/08/27/statement-press-secretary-review-group-intelligence-and-communications-t)
to oversee the current malefactors. [The review group then asked for
public
comment](http://icontherecord.tumblr.com/post/60323228143/review-group-on-global-signals-intelligence).
A group of technologists (including myself) [submitted a
comment](https://www.cdt.org/files/pdfs/nsa-review-panel-tech-comment.pdf)
demanding that the review group provide concrete technical details to
independent technologists.

Without knowing the specifics of how the various surveillance mechanisms
operate, the public in general can't make informed assessments about
what they should consider to be personally safe. And lack of detailed
technical knowledge also makes it much harder to mount an effective
political or legal opposition to the global surveillance state (e.g.
consider the terrible [Clapper v. Amnesty
International](http://www.supremecourt.gov/opinions/12pdf/11-1025_ihdj.pdf)
decision, where plaintiffs were denied standing to sue the Director of
National Intelligence because they could not demonstrate that they were
being surveilled).

It's also worth noting that the advocates for global surveillance do not
themselves want to be surveilled, and that (for example) the NSA has
tried to obscure as much of their operations as possible, by
over-classifying documents, and making spurious claims of "national
security". This is where the surveillance power dynamic is most baldly
in play, and many parts of the US government intelligence and military
apparatus has [a long
history](https://en.wikipedia.org/wiki/Pentagon_Papers) [of
acting](https://en.wikipedia.org/wiki/COINTELPRO) [in bad
faith](https://en.wikipedia.org/wiki/NSA_warrantless_surveillance_%282001%E2%80%9307%29)
to obscure its activities.

The people who have been operating these surveillance systems should be
ashamed of their work, and those who have been overseeing the operation
of these systems should be ashamed of themselves. We need to better
understand the scope of the damage done to our global infrastructure so
we can repair it if we have any hope of avoiding a complete surveillance
state in the future. Getting the technical details of these compromises
in the hands of the public is one step on the path toward a healthier
society.

### Postscript

Lest I be accused of optimism, let me make clear that fixing the
technical harms is necessary, but not sufficient; even if our technical
infrastructure had not been deliberately damaged, or if we manage to
repair it and stop people from damaging it again, far too many people
still regularly accept ubiquitous private (corporate) surveillance.
Private surveillance organizations (like Facebook and Google) are too
often in a position where their business interests are at odds with
their users' interests, and powerful adversaries can use a surveillance
organization as a lever against weaker parties.

But helping people to improve their own data sovereignty and to avoid
subjecting their friends and allies to private surveillance is a
discussion for a separate post, i think.

**Tags**:
[cryptography](https://debian-administration.org/tag/cryptography),
[nsa](https://debian-administration.org/tag/nsa)

</p>

