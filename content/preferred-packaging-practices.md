Title: Preferred Packaging Practices
Date: 2015-05-01 19:41
Author: Daniel Kahn Gillmor (dkg)
Slug: preferred-packaging-practices

I just took a few minutes to write up [my preferred Debian packaging
practices](https://wiki.debian.org/DanielKahnGillmor/preferred_packaging).

The basic gist is that i like to use `git-buildpackage` (`gbp`) with the
upstream source included in the repo, both as tarballs (with
`pristine-tar` branches) and including upstream's native VCS history
([Joey's arguments about syncing with upstream
git](http://joeyh.name/blog/entry/upstream_git_repositories/) are worth
reading if you're not already convinced this is a good idea).

I also started using `gbp-pq` recently -- the `patch-queue` feature is
really useful for at least three things:

-   rebasing your `debian/patches/` files when a new version comes out
    upstream -- you can use all your normal git rebase habits! and
-   facilitating sending patches upstream, hopefully reducing the
    divergence, and
-   cherry-picking new as-yet-unreleased upstream bugfix patches into a
    debian release.

My preferred packaging practices document is a work in progress. I'd
love to improve it. If you have suggestions, please let me know.

Also, if you've written up your own preferred packaging practices, send
me a link! I'm hoping to share and learn tips and tricks around this
kind of workflow [at debconf 15 this
year](https://summit.debconf.org/debconf15/meeting/194/git-buildpackage-skillshare/).

**Tags**: [git](https://debian-administration.org/tag/git),
[git-buildpackage](https://debian-administration.org/tag/git-buildpackage),
[packaging](https://debian-administration.org/tag/packaging)

</p>

