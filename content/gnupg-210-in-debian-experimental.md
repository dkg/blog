Title: GnuPG 2.1.0 in debian experimental
Date: 2014-11-06 23:27
Author: Daniel Kahn Gillmor (dkg)
Slug: gnupg-210-in-debian-experimental

Today, i uploaded GnuPG 2.1.0 into debian's experimental suite. It's
built for `amd64` and `i386` and `powerpc` already. You can [monitor its
progress on the
buildds](https://buildd.debian.org/status/package.php?p=gnupg2&suite=experimental)
to see when it's available for your architecture.

### Changes

[GnuPG 2.1 offers many new and interesting
features](https://gnupg.org/faq/whats-new-in-2.1.html), but one of the
most important changes is the introduction of elliptic curve crypto
(ECC). While GnuPG 2.1 discourages the creation of ECC keys by default,
it's important that we have the ability to verify ECC signatures and to
encrypt to ECC keys if other people are using this tech. It seems
likely, for example, that [Google's End-To-End Chrome OpenPGP
extension](https://code.google.com/p/end-to-end/) will use ECC. GnuPG
users who don't have this capability available won't be able to
communicate with End-To-End users.

There are many other architectural changes, including a move to more
daemonized interactions with the outside world, including using
`dirmngr` to talk to the keyservers, and relying more heavily on
`gpg-agent` for secret key access. The `gpg-agent` change is a welcome
one -- the agent now holds the secret key material entirely and never
releases it -- as of 2.1 `gpg2` never has any asymmetric secret key
material in its process space at all.

One other nice change for those of us with large keyrings is the new
keybox format for public key material. This provides much faster indexed
access to the public keyring.

I've been using GnuPG 2.1.0 betas regularly for the last month, and i
think that for the most part, they're ready for regular use.

### Timing for debian

The timing between the debian freeze and the GnuPG upstream is
unfortunate, but i don't think i'm prepared to push for this as a jessie
transition yet, without more backup. I'm talking to other members of the
GnuPG packaging team to see if they think this is worth even bringing to
the attention of the release team, but i'm not pursuing it at the
moment.

If you really want to see this in debian jessie, please install the
experimental package and let me know how it works for you.

### Long term migration concerns

GnuPG upstream is now maintaining three branches concurrently: modern
(2.1.x), stable (2.0.x), and classic (1.4.x). I think this is stretches
the GnuPG upstream development team too thin, and we should do what we
can to help them transition to supporting fewer releases concurrently.

In the long-term, I'd ultimately like to see gnupg 2.1.x to replace all
use of gpg 1.4.x and gpg 2.0.x in debian, but unlikely to to happen
right now.

In particular, the following two bugs make it impossible to use my
current, common monkeysphere workflow:

-   [`export-reset-subkey-passwd` doesn't
    work](https://bugs.g10code.com/gnupg/issue1753), which breaks
    `monkeysphere subkey-to-ssh-agent`.
-   [pluggable keyserver transports no longer
    work](https://bugs.g10code.com/gnupg/issue1754), which means that i
    can't use `hkpms://` access to keyservers.

And GnuPG 2.1.0 drops support for the older, known-weak OpenPGPv3 key
formats. This is an important step for simplification, but there are a
few people who probably [still need to use v3 keys for obscure/janky
reasons](http://lists.gnupg.org/pipermail/gnupg-devel/2014-November/029054.html),
or have data encrypted to a v3 key that they need to be able to decrypt.
Those people will want to have GnuPG 1.4 around.

### Call for testing

Anyway, if you use debian testing or unstable, and you are interested in
these features, i invite you to install \`gnupg2\` and its friends from
`experimental`. If you want to be sensibly conservative, i recommend
backing up \`\~/.gnupg\` before trying to use it:

    cp -aT .gnupg .gnupg.baksudo apt install -t experimental gnupg2 gnupg-agent dirmngr gpgsm gpgv2 scdaemon

If you find issues, please file them via the debian BTS as usual. I (or
other members of the pkg-gnupg team) will help you triage them to
upstream as needed.

**Tags**: [ecc](https://debian-administration.org/tag/ecc),
[experimental](https://debian-administration.org/tag/experimental),
[gnupg](https://debian-administration.org/tag/gnupg)

</p>

