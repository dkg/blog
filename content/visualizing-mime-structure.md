Title: visualizing MIME structure
Date: 2013-01-29 16:16
Author: Daniel Kahn Gillmor (dkg)
Slug: visualizing-mime-structure

Better debugging tools can help us understand what's going on with MIME
messages. A python scrap i wrote a couple years ago named
`printmimestructure` has been very useful to me, so i thought i'd share
it.

It reads a message from stdin, and prints a visualisation of its
structure, like this:

``` {.terminal}
0 dkg@alice:~$ printmimestructure < 'Maildir/cur/1269025522.M338697P12023.monkey,S=6459,W=6963:2,Sa' └┬╴multipart/signed 6546 bytes ├─╴text/plain inline 895 bytes └─╴application/pgp-signature inline [signature.asc] 836 bytes0 dkg@alice:~$ 
```

It is being published in the [notmuch](http://notmuchmail.org/)
repository, under `devel/`

    git clone git://notmuchmail.org/git/notmuchls -l notmuch/devel/printmimestructure

It feels silly to treat this \~30 line script as its own project, but i
don't know of another simple tool that does this. If you know of one, or
of something similar, i'd love to hear about it in the comments (or by
sending me e-mail if you prefer).

If it's useful for others, I'd be happy to contribute
`printmimestructure` to a project of like-minded tools. Does such a
project exist? Or if people think it would be handy to have in debian, i
can also package it up, though that feels like it might be overkill.

and oh yeah, as always: bug reports, suggestions, complaints, and
patches are welcome :)

(2013-10-09: updated to note change of location of the script, thanks to
the notmuch team for adopting it)

**Tags**: [debugging](https://debian-administration.org/tag/debugging),
[mime](https://debian-administration.org/tag/mime),
[python](https://debian-administration.org/tag/python)

</p>

