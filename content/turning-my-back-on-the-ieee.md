Title: Turning my back on the IEEE
Date: 2011-03-01 18:32
Author: Daniel Kahn Gillmor (dkg)
Slug: turning-my-back-on-the-ieee

I used to maintain a membership with [IEEE](http://ieee.org/). I no
longer do.

The latest nonsense to come from them is [a change for the worse in
their copyright assignment
policy](http://www.crypto.com/blog/copywrongs/), which i thought was
[already problematic](http://cr.yp.to/writing/ieee.html).

Other engineering societies (IETF, USENIX, etc) continue to do socially
relevant, useful work without attempting to control copyright of work
contributed to them. This just makes IEEE look like a power- and
money-hungry organization, rather than a force for positive advancement
of technology.

For shame, IEEE.

I will not consider reinstating my membership unless the organization
changes their copyright assignment and publication policy to better
reflect the spirit of scientific inquiry and technological advancement
they should stand for.

**Tags**: [freedom](https://debian-administration.org/tag/freedom),
[ieee](https://debian-administration.org/tag/ieee),
[policy](https://debian-administration.org/tag/policy)

</p>

