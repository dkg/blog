Title: keeping ssh host keys up-to-date with monkeysphere
Date: 2009-07-23 23:04
Author: Daniel Kahn Gillmor (dkg)
Slug: keeping-ssh-host-keys-up-to-date-with-monkeysphere

[Enrico](http://www.enricozini.org/) posted [a neat trick to track the
SSH host keys of debian
machines](http://www.enricozini.org/2009/debian/known-hosts/), thanks to
[Zobel](http://blog.zobel.ftbfs.de/). I wanted to mention
[monkeysphere](http://web.monkeysphere.info/), a project i'm involved
with which provides a more generalized structure for doing this kind of
update by taking advantage of the OpenPGP Web of Trust to distribute and
authenticate SSH keys.

Enrico's `known_hosts` update strategy is nice, but:

-   it's centralized,
-   it's useful only for debian developers (and only when connecting to
    debian machines),
-   it relies on users regularly running an update they wouldn't need to
    do otherwise (re-fetching the file from `master.debian.org` via
    `scp`) and
-   it relies on the maintainers of
    [master.debian.org](https://db.debian.org/machines.cgi?host=master):
    -   to avoid compromise (there are a ton of other problems if
        `master` was compromised, of course),
    -   to keep `/etc/ssh/ssh_known_hosts` up-to-date, and
    -   not to change the host key for `master` itself (or the user's
        regular `scp` updates would fail).

These are relatively small flaws, and as a project debian is able to
work around them because we have infrastructure in place like [the
machines database](https://db.debian.org/machines.cgi) (though checking
the machines db manually is tedious and therefore error-prone). But most
other projects don't have that level of organization, and the process
doesn't scale to other projects we (or our users) might be involved in.
And other projects (including debian, i'd think) might prefer to have a
less centralized process, to minimize bottlenecks and single points of
failure.

Check out Monkeysphere's [documentation for a server
administrator](http://web.monkeysphere.info/getting-started-admin/) for
a quick rundown about how to easily publish your SSH host keys via the
Web of Trust (it's not mutually-exclusive with the technique Enrico
describes).

And this is just part of what the monkeysphere can do: using the same
web of trust, monkeysphere is capable of helping a host authenticate
`ssh` users based on their OpenPGP identities, which gives full
re-keying and revocation functionality for these accounts. But that's a
separate discussion!

**Tags**:
[monkeysphere](https://debian-administration.org/tag/monkeysphere),
[openpgp](https://debian-administration.org/tag/openpgp),
[ssh](https://debian-administration.org/tag/ssh)

</p>

