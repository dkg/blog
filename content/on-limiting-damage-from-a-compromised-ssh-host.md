Title: on limiting damage from a compromised ssh host
Date: 2008-05-20 20:06
Author: Daniel Kahn Gillmor (dkg)
Slug: on-limiting-damage-from-a-compromised-ssh-host

[An interesting thread on
`openssh-unix-dev`](http://marc.info/?t=121067154000001&r=1&w=2) points
out a way that a compromised remote ssh account or host could
potentially trick users into divulging the passphrase for their local
secret key. Given that we're all dealing with the potential for
compromised ssh hosts lately, i think this is an important
consideration. (this has apparently been raised on `openssh-unix-dev` as
far back as [8 years ago](http://marc.info/?t=95066120400001&r=1&w=2)).

Unfortunately, a couple messages with mangled headers seem to have
broken the thread into
[two](http://marc.info/?t=121067154000001&r=1&w=2)
[chunks](http://marc.info/?t=121070538900003&r=1&w=2) in the archive,
severing the start of the thread from the part where it starts getting
really good: In particular, [Damien Miller's suggestion is to forbid
in-terminal key-based authentication entirely, and rely instead on
`ssh-agent`](http://marc.info/?l=openssh-unix-dev&m=121124814818346&w=2).
My impression of this strategy is that it's analogous to forcing an
out-of-band verification process -- since the in-channel communication
is happening in the tty, and the remote host will have some level of
control over the tty once authentication succeeds, it's important that
any locally-divulged secrets (e.g. the passphrase for the local secret
key) *are not* transmitted over the tty in question.

Of course, disabling in-terminal key-based authentication creates
something of a usability problem for users who aren't using X11 (and who
don't have some other method for an out-of-band `ssh-askpass`). And it
also points out the additional problems with X11 forwarding (e.g. a
compromised host allowed to forward X connections could trigger a
mimicry of a standard `ssh-askpass` password prompt). This is something
that debian might want to consider, as we've diverged from upstream on
the default settings for `ForwardX11Trusted`.

I'm uncomfortable with how Damien's suggestion raises the bar to
key-based authentication in general, since users will now have to
understand both keys and agents in order to effectively authenticate
this way. But maybe that's what's needed, since we desperately need to
phase out password-based authentication (and all the security pitfalls
associated with it) in this brave new networked world.

At any rate, i've adopted the suggestion for now:

    [0 dkg@squeak ~]$ tail -n1 .ssh/configIdentityFile none[0 dkg@squeak ~]$ 

I'm curious to know if other people have adopted this strategy, or have
other mitigating techniques.

**Tags**: [security](https://debian-administration.org/tag/security),
[ssh](https://debian-administration.org/tag/ssh)

</p>

