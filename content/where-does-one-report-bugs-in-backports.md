Title: Where does one report bugs in backports?
Date: 2006-09-05 06:05
Author: Daniel Kahn Gillmor (dkg)
Slug: where-does-one-report-bugs-in-backports

initramfs-tools 0.77b\~bpo.1 arrived in sarge-backports recently. It
appears to Depend: on klibc-utils (&gt;= 1.4.19-2), but should probably
depend on klibc-utils (&gt;= 1.4.19-2\~bpo.1) instead, since it's
otherwise uninstallable on a sarge/sarge-backports system.  
  
Who should i report this problem to? it seems like filing it as a
regular debian bug would amount to clutter on the BTS, but i can't find
a comparable system for bpo.  
  
i just asked on \#debian-backports, but i've gotten no response there in
my (admittedly short) attention span. the Maintainers: field is just the
kernel team, who i doubt deserves to be nagged about sarge-backports,
busy as they are.  

