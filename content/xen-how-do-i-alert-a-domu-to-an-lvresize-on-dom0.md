Title: Xen: how do i alert a domU to an lvresize on dom0?
Date: 2007-06-19 22:36
Author: Daniel Kahn Gillmor (dkg)
Slug: xen-how-do-i-alert-a-domu-to-an-lvresize-on-dom0

I have a simple Xen setup with two domains, a `dom0` and a `domU`. The
physical disks are handled by LVM in the `dom0`, and certain logical
volumes from the `dom0` are exported to the `domU` as `/dev/sda1`, etc.

If i resize one of the logical volumes from the `dom0`, how can i
convince the `domU` to learn of the change in the underlying size? Is it
possible to do this without restarting the `domU`? I haven't had any
luck searching the 'net.

On the `domU`,i saw that the block device in question is 10G:

    0 domU:~# cat /proc/partitions major minor  #blocks  name   8     1   10485760 sda1   8     2     524288 sda2   8     3   10485760 sda3 253     0   10485244 dm-00 domU:~# 

and on the `dom0`, i check that it looks right, and then `lvresize` it:

    0 dom0:~# lvs  LV         VG   Attr   LSize   Origin Snap%  Move Log Copy%   domU-disk  vg0  -wi-ao  10.00G                                domU-srv   vg0  -wi-ao  10.00G                                domU-swap  vg0  -wi-ao 512.00M                                dom0       vg0  -wi-ao   1.00G                              0 dom0:~# lvresize --size +2G vg0/domU-srv  Extending logical volume domU-srv to 12.00 GB  Logical volume domU-srv successfully resized0 dom0:~# 

but `domU:/proc/partitions` still looks the same. I've tried installing
`hdparm` on the `domU` and creating a fake `/dev/sda` for it to "re-read
the partition table from", but that didn't work:

    0 domU:~# mknod /dev/sda b 8 10 domU:~# hdparm -z /dev/sda/dev/sda: BLKRRPART failed: Invalid argument0 domU:~# cat /proc/partitions major minor  #blocks  name   8     1   10485760 sda1   8     2     524288 sda2   8     3   10485760 sda3 253     0   10485244 dm-00 domU:~# 

any suggestions for what else i should try? Is there a xen-specific
command to alert the `domU` of such a change?

**Tags**: [lvm](https://debian-administration.org/tag/lvm),
[xen](https://debian-administration.org/tag/xen)

</p>

