Title: Make a Woolly Mammoth (thanks, inkscape!)
Date: 2013-02-28 00:15
Author: Daniel Kahn Gillmor (dkg)
Slug: make-a-woolly-mammoth-thanks-inkscape

I feel like i've done a lot of blogging recently about failing to do
things with proprietary software. That's annoying.

This post is about something i made successfully with free software (and
some non-software crafting): I made a Woolly Mammoth for my
nephew!<img></img>

I documented [the pattern (with pictures!) that i came up
with](https://dkg.fifthhorseman.net/woolly/) using
[Inkscape](http://inkscape.org/) (and used markdown, pandoc, emacs,
pdftk, and other free software in the process). i've also published the
source for the pattern via git if you want to modify it:

    git clone git://lair.fifthhorseman.net/~dkg/woolly

Writing up the documentation makes me realize that i don't know of any
software tools designed specifically for facilitating fabric/craft
construction. Some interesting software ideas:

-   Make 3-D models showing the partly assembled pieces, derived from
    the flat pattern. Maybe something like
    [blender](http://blender.org/) would be good for this?
-   Take a 3D-modeled form and produce some candidate patterns for
    cutting and sewing? This seems like it is an interesting theoretical
    problem: given a set of (marked?) 3D surfaces and a set of
    approximation constraints, have the tool come up with a reasonable
    set of 2D patterns that could be cut and assembled using a set of
    standard operations into something close to the 3D shape.
-   a "pattern lint checker" (maybe an inkscape extension?) that would
    let you mark certain segments of an SVG as related to other segments
    (i.e. the two sides of a seam), and could give you warnings when one
    side was longer than the other (within some level of tolerance)

Anyone have any ideas?

**Tags**:
[brainstorming](https://debian-administration.org/tag/brainstorming),
[crafting](https://debian-administration.org/tag/crafting),
[inkscape](https://debian-administration.org/tag/inkscape)

</p>

